Users module for Yii2.
==================
This module provide a users managing system for your yii2 application.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
composer require --prefer-dist interpro/yii2-userModule "*"
```

or add

```
"interpro/yii2-userModule" : "dev-master"
```

and 

```
    "repositories": [{
        "type": "vcs",
         "url": "https://gitlab.com/interPro-yii2/userModule.git"
        }
    ]
```

to the require section of your `composer.json` file

Configuration
-------------

Add `yii2-userModule` to `module` section of each application config:

```php
'modules' => [
    'user' => [
        'class' => 'interPro\userModule\backend\Module',
    ],
],
```

Add or edit `user` component section:

```php
'user' => [
    'identityClass' => 'interPro\userModule\common\models\User',
]
```

Run module migration:

```php
php yii migrate --migrationPath=@vendor/interpro/yii2-userModule/src/console/migrations
```

Usage
-----

Once the extension is installed, simply use it in your code by:

```php
Yii::$app->getModule('user');
```

### Backend ###
@todo

Dependences
-----------
- [yii2](https://github.com/yiisoft/yii2)