<?php

namespace interPro\userModule\backend;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'interPro\userModule\backend\controllers';
    public $defaultRoute = 'user';
    public $rbacEnabled = false;

    public function init()
    {
        parent::init();
        // Add module URL rules.
        Yii::$app->urlManager->addRules(
            [
                'users/<action>' => 'user/user/<action>'
            ],
            false
        );
        
        $this->registerTranslations();
    }
    
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['user'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@vendor/interpro/yii2-userModule/src/common/messages'
        ];
        
        Yii::$app->i18n->translations['address'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@vendor/interpro/yii2-userModule/src/common/messages'
        ];
    }
}
