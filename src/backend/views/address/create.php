<?php

use yii\helpers\Html;
use interPro\userModule\common\models\User;


/* @var $this yii\web\View */
/* @var $model common\models\Address */

$user = User::findOne($model->idUser);

$this->title = Yii::t('address', 'Create Address');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->firstName . ' ' . $user->lastName, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
