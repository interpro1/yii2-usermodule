<?php

use yii\helpers\Html;
use interPro\userModule\common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\Address */

$user = User::findOne($model->idUser);

$this->title = Yii::t('address', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('address','Address'),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->firstName . ' ' . $user->lastName, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('crm', 'Update');
?>
<div class="address-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
