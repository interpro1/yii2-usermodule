<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use interPro\userModule\common\models\Address;
use interPro\userModule\common\dictionary\Countries;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Address */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="address-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="row">
        <div class="col-md-4">

            <?= $form->field($model, 'type')->dropDownList(Address::getTypesList()) ?>
            <?= $form->field($model, 'nip') ?>
            <?= $form->field($model, 'phone') ?>
        </div>
        <div class="col-md-7 col-md-offset-1">
            <?= $form->field($model, 'name') ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'street') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'number') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'apartmentNumber') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <?= $form->field($model, 'city') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'postCode') ?>
                </div>
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'country')->widget(Select2::classname(), [
                        'data' => Countries::getCountriesWithNativeNames(),
                        'options' => ['placeholder' => Yii::t('address', 'Select country')],
                    ]);
                    $form->field($model, 'country')
                    ?>
                </div>
            </div>
        </div>


    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('user', 'Create') : Yii::t('address', 'Save changes'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
