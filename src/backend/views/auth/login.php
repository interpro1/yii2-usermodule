<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('user', 'Login');
?>
<div class="site-login">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <div class="form-group pull-right">
                    <?= Html::submitButton(Yii::t('user', 'Login'), ['class' => 'btn btn-primary btn-lg', 'name' => 'login-button']) ?>
                </div>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
