<?php

/**
 * @var $model \interPro\userModule\common\models\User
 */

/* @var $this yii\web\View */
/* @var $model interPro\userModule\common\models\User */

$this->title = Yii::t('user', 'User permission');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->firstName . ' ' . $model->lastName, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= $this->title ?>: <?= $model->firstName . ' ' . $model->lastName ?></h1>
<?php
echo \dektrium\rbac\widgets\Assignments::widget([
    'userId' => $model->id
]);

echo '<br>';

echo \yii\helpers\Html::a(Yii::t('user', 'Return to user view'), ['view', 'id' => $model->id], [
        'class' => 'btn btn-primary'
]);