<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use interPro\userModule\common\models\User;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model interPro\userModule\common\models\User */

$this->title = $model->firstName . ' ' . $model->lastName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p class="pull-right">
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('user', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php

        if ($model->status !== User::STATUS_ACTIVE) {
            echo Html::a('<span class="glyphicon glyphicon-ok"></span> ' . Yii::t('user', 'User activation'), ['activate', 'id' => $model->id], [
                    'class' => 'btn btn-warning',
                    'data' => [
                        'confirm' => Yii::t('user', 'Are you sure you want to activate this user?'),
                        'method' => 'post',
                    ],
                ]) . ' ';
        }

        if ($model->userType === User::TYPE_CUSTOMER) {
            echo Html::a('<span class="glyphicon glyphicon-star"></span> ' . Yii::t('user', 'Set as admin'), ['set-as-admin', 'id' => $model->id], [
                'class' => 'btn btn-success',
                'data' => [
                    'confirm' => Yii::t('user', 'Are you sure you want to set as admin this user?'),
                    'method' => 'post',
                ],
            ]);
        } else {
            echo Html::a('<span class="glyphicon glyphicon-user"></span> ' . Yii::t('user', 'Set as customer'), ['set-as-customer', 'id' => $model->id], [
                'class' => 'btn btn-warning',
                'data' => [
                    'confirm' => Yii::t('user', 'Are you sure you want to set as customer this user?'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>
        <?php
        if (Yii::$app->getModule('user')->rbacEnabled) {
            echo Html::a('<span class="glyphicon glyphicon glyphicon-wrench"></span> ' . Yii::t('user', 'Edit permissions'), ['assign', 'id' => $model->id], ['class' => 'btn btn-default']);
        }
        ?>
        <?=
        Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('user', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('user', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'firstName',
            'lastName',
            'email:email',
            [
                'label' => Yii::t('all', 'Status'),
                'value' => User::getStatusesList()[$model->status]
            ],
            'language',
            'createdDate',
            'updatedDate',
            'lastLogin'
        ],
    ]);

    echo Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('address', 'Create address'), ['address/create', 'idUser' => $model->id], ['class' => 'pull-right btn btn-default']);

    echo '<h3>' . Yii::t('user', 'Addresses user') . '</h3>';

    $dataAddressProvider = new ActiveDataProvider([
        'query' => $model->getAddresses()
    ]);

    echo GridView::widget([
        'dataProvider' => $dataAddressProvider,
        'layout' => "{items}\n{pager}",
        'columns' => [
            [
                'attribute' => 'user',
                'label' => Yii::t('user', 'Type'),
                'format' => 'html',
                'value' => function ($model) {
                    return $model->getType();
                },
            ],
            'name',
            [
                'attribute' => 'user',
                'label' => Yii::t('address', 'Address'),
                'format' => 'html',
                'value' => function ($model) {
                    return $model->getPreformatted();
                },
            ],
            'nip',
            'phone',
            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'address',
                'template' => '{update} {delete}'
            ]
        ],
    ]);
    ?>

</div>
