<?php

use yii\helpers\Html;
use yii\grid\GridView;
use interPro\userModule\common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel interPro\userModule\backend\models\userSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('user', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <p class="pull-right">
        <?= Html::a(Yii::t('user', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'id',
                'filter' => '',
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'userType',
                'filter' => User::getTypesList(),
                'format' => 'html',
                'value' => function ($data) {
                    $icon = $data->userType === User::TYPE_ADMIN ? 'star' : 'user';
                    return '<span class="glyphicon glyphicon-' . $icon . '" title="' . User::getTypesList()[$data->userType] . '"></span>';
                },
            ],
            'username',
            'firstName',
            'lastName',
            'email:email',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => User::getStatusesList(),
                'value' => function ($data) {
                    return User::getStatusesList()[$data->status];
                }
            ],
            'language',
            'createdDate',
            'lastLogin',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Yii::$app->getModule('user')->rbacEnabled ? '{view} {assign} {update} {delete}' : '{view} {update} {delete}',
                'buttons' => [
                    'assign' => function ($url, $model, $key) {
                        return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-wrench']), ['assign', 'id' => $model->id], [
                            'title' => Yii::t('user', 'Edit permissions'),
                            'aria-label' => Yii::t('user', 'Edit permissions'),
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>

</div>
