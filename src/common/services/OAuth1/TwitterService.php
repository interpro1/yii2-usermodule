<?php

namespace interPro\userModule\common\services\OAuth1;

use interPro\userModule\common\services\OAuthServiceInterface;
use nodge\eauth\services\extended\TwitterOAuth1Service;

class TwitterService extends TwitterOAuth1Service implements OAuthServiceInterface
{
    protected function fetchAttributes()
    {
        $info = $this->makeSignedRequest('account/verify_credentials.json');

        $this->attributes['id'] = $info['id'];
        $this->attributes['name'] = $info['name'];

        $this->attributes['username'] = $info['screen_name'];
        $this->attributes['language'] = $info['lang'];
        $this->attributes['timezone'] = timezone_name_from_abbr('', $info['utc_offset'], date('I'));
        $this->attributes['profile_picture'] = $info['profile_image_url'];

        return true;
    }

    /**
     * @return string
     */
    public function getPhoto() {
        return $this->getAttribute('profile_picture');
    }

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->getAttribute('first_name');
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->getAttribute('last_name');
    }

    /**
     * Twitter Api not return email.
     * @return null
     */
    public function getEmail()
    {
        return null;
    }
}