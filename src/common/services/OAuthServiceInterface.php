<?php

namespace interPro\userModule\common\services;

use nodge\eauth\IAuthService;

interface OAuthServiceInterface extends IAuthService{
    public function getPhoto();

    public function getFirstName();
    public function getLastName();

    public function getEmail();
}