<?php

namespace interPro\userModule\common\services\OAuth2;

use interPro\userModule\common\services\OAuthServiceInterface;
use nodge\eauth\services\GoogleOAuth2Service;

class GoogleService extends GoogleOAuth2Service implements OAuthServiceInterface
{

    protected $scopes = [parent::SCOPE_USERINFO_PROFILE, parent::SCOPE_EMAIL];

    protected function fetchAttributes()
    {
        $info = $this->makeSignedRequest('https://www.googleapis.com/oauth2/v1/userinfo');

        $this->attributes['id'] = $info['id'];
        $this->attributes['full_name'] = $info['name'];
        $this->attributes['username'] = $info['name'];

        if (!empty($info['link'])) {
            $this->attributes['url'] = $info['link'];
        }

        $this->attributes['profile_picture'] = $info['picture'];
        $this->attributes['email'] = $info['email'];

        return true;
    }

    /**
     * @return string
     */
    public function getPhoto() {
        return $this->getAttribute('profile_picture');
    }

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->getAttribute('first_name');
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->getAttribute('last_name');
    }

    /**
     * Instagram Api not return email.
     * @return null
     */
    public function getEmail()
    {
        return $this->attributes['email'];
    }
}