<?php

namespace interPro\userModule\common\services\OAuth2;

use interPro\userModule\common\services\OAuthServiceInterface;
use nodge\eauth\services\FacebookOAuth2Service;

class FacebookService extends FacebookOAuth2Service implements OAuthServiceInterface
{
    protected $scopes = [
        self::SCOPE_EMAIL
    ];

    /**
     * http://developers.facebook.com/docs/reference/api/user/
     *
     * @see FacebookOAuth2Service::fetchAttributes()
     */
    protected function fetchAttributes()
    {
        $this->attributes = $this->makeSignedRequest('me', [
            'query' => [
                'fields' => join(',', [
                    'id',
                    'name',
                    'email'
                ])
            ]
        ]);

        if (!isset($this->attributes['email'])) {
            $this->attributes['email'] = null;
        }

        $this->attributes['username'] = $this->attributes['name'];
        $this->attributes['photo_url'] = $this->baseApiUrl.$this->attributes['id'].'/picture?width=100&height=100';

        return true;
    }

    /**
     * @return string
     */
    public function getPhoto() {
        return $this->getAttribute('photo_url');
    }

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->getAttribute('first_name');
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->getAttribute('last_name');
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->attributes['email'];
    }

    /**
     * Returns the array that contains all available authorization attributes.
     *
     * @return array the attributes.
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
}