<?php

namespace interPro\userModule\common\services\OAuth2;

use interPro\userModule\common\services\OAuthServiceInterface;
use nodge\eauth\services\InstagramOAuth2Service;

class InstagramService extends InstagramOAuth2Service implements OAuthServiceInterface
{
    /**
     * @return string
     */
    public function getPhoto() {
        return $this->getAttribute('profile_picture');
    }

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->getAttribute('first_name');
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->getAttribute('last_name');
    }

    /**
     * Instagram Api not return email.
     * @return null
     */
    public function getEmail()
    {
        return null;
    }
}