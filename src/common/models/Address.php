<?php

namespace interPro\userModule\common\models;

use Yii;
use yii\db\ActiveRecord;
use interPro\userModule\common\dictionary\Countries;
use yii\helpers\Html;

/**
 * This is the model class for table "addresses".
 *
 * @property integer $id
 * @property string $name
 * @property string $nip
 * @property string $country
 * @property string $city
 * @property string $postCode
 * @property string $street
 * @property string $number
 * @property string $apartmentNumber
 * @property string $phone
 * @property integer $idUser
 * @property integer $type
 * @property integer $orderId
 *
 * @property Users $user
 */
class Address extends ActiveRecord {

    const TYPE_COMPANY = 0;
    const TYPE_SHIPPMENT = 1;
    const TYPE_PAYMENT = 2;
    const SCENARIO_COMPANY_INVOICE = 'company_invoice';
    const SCENARIO_SHIPPING = 'shipping';
    const SCENARIO_COMPANY = 'company';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%addresses}}';
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_COMPANY_INVOICE] = ['name', 'city', 'idUser', 'type', 'nip'];
        $scenarios[self::SCENARIO_COMPANY] = ['name', 'city', 'idUser', 'type'];
        $scenarios[self::SCENARIO_SHIPPING] = ['name', 'city', 'idUser', 'type', 'phone'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'city', 'idUser', 'type'], 'required'],
            [['idUser', 'type', 'orderId'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['nip'], 'string', 'max' => 20],
            [['phone'], 'string', 'max' => 15],
            [['country'], 'string', 'max' => 2],
            ['country', 'default', 'value' => 'PL'],
            [['city'], 'string', 'max' => 100],
            [['postCode', 'number', 'apartmentNumber'], 'string', 'max' => 10],
            [['street'], 'string', 'max' => 150],
            [
                'idUser', 'exist',
                'targetAttribute' => 'id',
                'targetClass' => 'interPro\userModule\common\models\User',
                'message' => Yii::t('address', 'User does not exist.')
            ],
            [['nip'], 'required', 'on' => self::SCENARIO_COMPANY_INVOICE],
            [['phone'], 'required', 'on' => self::SCENARIO_SHIPPING],
            ['type', 'default', 'value' => self::TYPE_COMPANY],
            ['type', 'in', 'range' => [self::TYPE_COMPANY, self::TYPE_SHIPPMENT, self::TYPE_PAYMENT]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('address', 'ID'),
            'name' => Yii::t('address', 'Recipient'),
            'nip' => Yii::t('address', 'Nip'),
            'country' => Yii::t('address', 'Country'),
            'city' => Yii::t('address', 'City'),
            'postCode' => Yii::t('address', 'Post Code'),
            'street' => Yii::t('address', 'Street'),
            'number' => Yii::t('address', 'Number'),
            'apartmentNumber' => Yii::t('address', 'Apartment Number'),
            'phone' => Yii::t('address', 'Phone'),
            'idUser' => Yii::t('address', 'Id User'),
            'type' => Yii::t('address', 'Type')
        ];
    }

    public function getPreformatted() {

        $number = $this->apartmentNumber ? $this->number . '/' . $this->apartmentNumber : $this->number;
        return $this->postCode . ' '
                . $this->city . Html::tag('br')
                . $this->street . ' '
                . $number . '<br />'
                . (is_string($this->country) ? Countries::getName($this->country) : '');
    }

    public function getType() {
        switch ($this->type) {
            case self::TYPE_PAYMENT:
                return Yii::t('address', 'Payment address');
            case self::TYPE_SHIPPMENT:
                return Yii::t('address', 'Shippment address');

            case self::TYPE_COMPANY:
            default:
                return Yii::t('address', 'Main address');
        }
    }

    /**
     * Gets list of types
     * @return array 
     */
    public static function getTypesList() {
        return [
            self::TYPE_COMPANY => Yii::t('address', 'Main address'),
            self::TYPE_PAYMENT => Yii::t('address', 'Payment address'),
            self::TYPE_SHIPPMENT => Yii::t('address', 'Shippment address'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'idUser']);
    }

    public function beforeValidate() {
        if (parent::beforeValidate()) {
            switch ($this->type) {
                case self::TYPE_PAYMENT:
                    $this->setScenario(self::SCENARIO_COMPANY_INVOICE);
                    break;
                case self::TYPE_SHIPPMENT:
                    $this->setScenario(self::SCENARIO_SHIPPING);
                    break;
                default :
                    $this->setScenario(self::SCENARIO_COMPANY);
                    break;
            }

            return true;
        }

        return false;
    }

}
