<?php

namespace interPro\userModule\common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
use interPro\userModule\common\models\Address;

/**
 * User model
 *
 * Author Tomasz Górecki <tomas.gorecki@gmail.com>
 *
 * @property integer $id
 * @property string $username
 * @property string $firstName
 * @property string $lastName
 * @property string $passwordHash
 * @property string $passwordResetToken
 * @property string $email
 * @property string $phone
 * @property string $authKey
 * @property integer $status
 * @property datatime $createdDate
 * @property datatime $updatedDate
 * @property datatime $lastLogin
 * @property string $language
 * @property integer $userType
 * @property string $socialId
 * @property string $photo
 *
 * @property Address $address
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const TYPE_CUSTOMER = 1;
    const TYPE_TEMPORARY = 2;
    const TYPE_ADMIN = 9;
    const SCENARIO_SOCIAL = 'social';
    const SCENARIO_TEMPORARY = 'temporary';

    const EMAIL_USER_ACTIVATED = 'user-activated';
    const TEMPORARY_USER_KEY = 'temporaryUser';

    public $password;
    public $repeatpassword;

    /**
     * @var array EAuth attributes
     */
    public $profile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdDate',
                'updatedAtAttribute' => 'updatedDate',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'message' => Yii::t('user', 'This username has already been taken.'), 'on' => self::SCENARIO_DEFAULT],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'filter' => ['userType' => self::TYPE_CUSTOMER], 'message' => Yii::t('user', 'This email address has already been taken.'),
                'when' => function ($model, $attribute) {
                    return $model->{$attribute} === self::TYPE_CUSTOMER;
                }],

            ['phone', 'string', 'min' => 9, 'max' => 15],
            ['phone', 'required', 'on' => [self::SCENARIO_TEMPORARY]],

            ['password', 'required', 'on' => ['register', 'create']],
            ['password', 'string', 'min' => 6],

            ['repeatpassword', 'required', 'on' => ['register', 'create']],
            ['repeatpassword', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('user', "Passwords don't match.")],

            ['userType', 'default', 'value' => self::TYPE_CUSTOMER],
            ['userType', 'in', 'range' => [self::TYPE_CUSTOMER, self::TYPE_TEMPORARY, self::TYPE_ADMIN]],

            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['firstName', 'required'],
            ['firstName', 'string', 'min' => 2, 'max' => 255],
            ['lastName', 'required'],
            ['lastName', 'string', 'min' => 2, 'max' => 255],
            ['socialId', 'string', 'min' => 2, 'max' => 100],
            ['socialId', 'required', 'on' => [self::SCENARIO_SOCIAL]],

            ['photo', 'string', 'min' => 2, 'max' => 500]
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'role' => Yii::t('user', 'Role'),
            'username' => Yii::t('user', 'Username'),
            'firstName' => Yii::t('user', 'First Name'),
            'lastName' => Yii::t('user', 'Last Name'),
            'password' => Yii::t('user', 'Password'),
            'repeatpassword' => Yii::t('user', 'Repeat password'),
            'email' => Yii::t('user', 'E-mail'),
            'phone' => Yii::t('user', 'Phone'),
            'status' => Yii::t('user', 'Status'),
            'createdDate' => Yii::t('user', 'Registration date'),
            'updatedDate' => Yii::t('user', 'Last changed date'),
            'lastLogin' => Yii::t('user', 'Last login date'),
            'userType' => Yii::t('user', 'Acoount type'),
            'language' => Yii::t('user', 'Language')
        ];
    }


    /**
     * Returns a list of scenarios and the corresponding active attributes.
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DEFAULT] = ['id', 'username', 'email', 'phone', 'firstName', 'lastName', 'status'];
        $scenarios['register'] = ['username', 'email', 'phone', 'firstName', 'lastName', 'password', 'repeatpassword'];
        $scenarios['create'] = ['username', 'role', 'email', 'phone', 'firstName', 'lastName', 'password', 'repeatpassword'];
        $scenarios['update'] = ['email', 'username', 'role', 'phone', 'firstName', 'lastName', 'password', 'repeatpassword', 'status'];
        $scenarios[self::SCENARIO_TEMPORARY] = ['email', 'firstName', 'lastName', 'phone', 'userType'];
        $scenarios[self::SCENARIO_SOCIAL] = ['username', 'socialId'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        if (Yii::$app->getSession()->has('user-' . $id)) {
            return new self(Yii::$app->getSession()->get('user-' . $id));
        } else {
            return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['authKey' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'socialId' => null, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findAdminByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE, 'userType' => self::TYPE_ADMIN]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'passwordResetToken' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->passwordHash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->passwordHash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->passwordResetToken = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->passwordResetToken = null;
    }

    /**
     * Set last login for user.
     * @return boolean
     */
    public function setLastLogin()
    {
        $this->lastLogin = new Expression('NOW()');

        if (!$this->isNewRecord) {
            //I didn't use save() because don't wont change updateDate
            return (bool)$this->updateAttributes($this);
        } else {
            return $this;
        }
    }

    /**
     * Sets random password
     * @param int $length Length of password
     */
    public function resetPassword($length = 8)
    {
        $this->setPassword(Yii::$app->security->generateRandomString($length));
    }

    public function validateRepeatPassword()
    {
        return $this->password === $this->password2;
    }

    /**
     * Gets list of user statuses
     * @return array
     */
    public static function getStatusesList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('user', 'Active'),
            self::STATUS_DELETED => Yii::t('user', 'Deleted'),
        ];
    }

    /**
     * Gets list of user types
     * @return array
     */
    public static function getTypesList()
    {
        return [
            self::TYPE_CUSTOMER => Yii::t('user', 'Customer'),
            self::TYPE_ADMIN => Yii::t('user', 'Admin'),
            self::TYPE_TEMPORARY => Yii::t('user', "User temporary")
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($insert) {
                $this->setPassword($this->password);
                if ($this->scenario === 'register' || $this->scenario = self::SCENARIO_TEMPORARY) {
                    $this->generateAuthKey();
                    $this->setLastLogin();
                }
                $this->language = Yii::$app->language;
            } elseif ($this->scenario === 'update' && $this->password) {
                $this->setPassword($this->password);
            }
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert && $this->status !== self::STATUS_ACTIVE && $this->userType === self::TYPE_CUSTOMER) {

            return Yii::$app->mailer->compose(['text' => '@vendor/interpro/yii2-userModule/src/common/mail/text/admin-user-created'], [
                'user' => $this,
            ])
                ->setFrom([\Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo(\Yii::$app->params['adminEmail'])
                ->setSubject(Yii::t('user', 'New user account on the system'))
                ->send();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['idUser' => 'id'])->where(['orderId' => null]);
    }

    public function sendEmail($type)
    {
        try {
            return Yii::$app->mailer->compose(['text' => '@vendor/interpro/yii2-userModule/src/common/mail/text/' . $type], [
                'user' => $this
            ])
                ->setFrom([\Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($this->email)
                ->setSubject(Yii::t('user', $type))
                ->send();
        } catch (\Exception $exc) {
            Yii::$app->session->setFlash('error', Yii::t('user', 'An error occurred while sending the message.'));
        }
    }


    /**
     * @param \nodge\eauth\ServiceBase $service
     * @return User
     * @throws ErrorException
     */
    public static function findByEAuth($service)
    {
        if (!$service->getIsAuthenticated()) {
            throw new ErrorException('EAuth user should be authenticated before creating identity.');
        }

        $id = $service->getServiceName() . '-' . $service->getId();

        $user = static::findOne(['socialId' => $id]);
        if (is_null($user)) {
            $attributes = [
                'username' => $service->getAttribute('username'),
                'authKey' => md5($id),
                'profile' => $service->getAttributes(),
                'firstName' => $service->getFirstName(),
                'lastName' => $service->getLastName(),
                'socialId' => $id,
                'photo' => $service->getPhoto(),
                'email' => $service->getEmail()
            ];
            $user = new self($attributes);
            $user->setScenario(self::SCENARIO_SOCIAL);
            $user->save();
        }
        $attributes['profile']['service'] = $service->getServiceName();
        Yii::$app->getSession()->set('user-' . $id, $attributes);
        return $user;
    }

    public function isSocialUser()
    {
        if (is_null($this->getAttribute('socialId'))) {
            return false;
        }

        return true;
    }


    public function loginRequired($checkAjax = true)
    {

        die('test');
        $request = Yii::$app->getRequest();
        if ($this->enableSession && (!$checkAjax || !$request->getIsAjax())) {
            $this->setReturnUrl($request->getUrl());
        }
        if ($this->loginUrl !== null) {
            $loginUrl = (array)$this->loginUrl;
            if ($loginUrl[0] !== Yii::$app->requestedRoute) {
                return Yii::$app->getResponse()->redirect($this->loginUrl);
            }
        }
        throw new ForbiddenHttpException(Yii::t('yii', 'Login Required'));
    }
}
