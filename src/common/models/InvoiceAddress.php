<?php

namespace interPro\userModule\common\models;


class InvoiceAddress extends Address
{
    public function rules()
    {
        $rules = [
            [['name', 'city'], 'required', 'whenClient' => "function (attribute, value) {
                return $('#add-invoice').is(\":checked\")
            }"],
        ];
        return $rules + parent::rules();
    }
}