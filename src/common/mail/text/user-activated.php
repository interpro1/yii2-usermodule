<?php

/* @var $this yii\web\View */
/* @var $user interPro\userModule\common\models\User */

echo Yii::t('user', 'Thank you for registering on our website.') . "\n\n";

echo Yii::t('user', 'Your account has been properly verified. From this moment you can benefit from our website by logging in using the data given during registration.') . "\n\n";

echo Yii::t('user', 'Your username is:') . ' ' . $user->username; 