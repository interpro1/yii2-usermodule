<?php

/* @var $this yii\web\View */
/* @var $user interPro\userModule\common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/reset-password', 'token' => $user->passwordResetToken]);
?>
<?= Yii::t('user', 'Hello'); ?> <?= $user->username ?>,

<?= Yii::t('user', 'Follow the link below to reset your password:'); ?>

<?= $resetLink ?>
