<?php


/* @var $this yii\web\View */
/* @var $user interPro\userModule\common\models\User */
/* @var $address interPro\userModule\common\models\Address */

$attributes = [
    'username',
    'firstName',
    'lastName',
    'email',
    'phone',
];

echo Yii::t('user', 'Has been created a new user account on the system.') . "\n\n";

foreach ($attributes as $attribute) {
    echo $user->getAttributeLabel($attribute) . ': ' . $user->{$attribute} . "\n";
}

echo "\n" . Yii::t('user', 'User account waiting for acceptance.');
