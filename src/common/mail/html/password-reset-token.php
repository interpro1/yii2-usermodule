<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user interPro\userModule\common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/reset-password', 'token' => $user->passwordResetToken]);
?>
<div class="password-reset">
    <p><?= Yii::t('user', 'Hello'); ?> <?= Html::encode($user->username) ?>,</p>

    <p><?= Yii::t('user', 'Follow the link below to reset your password:'); ?></p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
