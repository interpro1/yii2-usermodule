<?php

namespace interPro\userModule\common\dictionary;

class Countries {
    private static $countries = [
        "AF" => array('eng' => "Afghanistan", 'native' => "‫افغانستان"),
        "AX" => array('eng' => "Åland Islands", 'native' => "Åland"),
        "AL" => array('eng' => "Albania", 'native' => "Shqipëri"),
        "DZ" => array('eng' => "Algeria", 'native' => "‫الجزائر"),
        "AS" => array('eng' => "American Samoa", 'native' => ""),
        "AD" => array('eng' => "Andorra", 'native' => ""),
        "AO" => array('eng' => "Angola", 'native' => ""),
        "AI" => array('eng' => "Anguilla", 'native' => ""),
        "AQ" => array('eng' => "Antarctica", 'native' => ""),
        "AG" => array('eng' => "Antigua and Barbuda", 'native' => ""),
        "AR" => array('eng' => "Argentina", 'native' => ""),
        "AM" => array('eng' => "Armenia", 'native' => "Հայաստան"),
        "AW" => array('eng' => "Aruba", 'native' => ""),
        "AC" => array('eng' => "Ascension Island", 'native' => ""),
        "AU" => array('eng' => "Australia", 'native' => ""),
        "AT" => array('eng' => "Austria", 'native' => "Österreich"),
        "AZ" => array('eng' => "Azerbaijan", 'native' => "Azərbaycan"),
        "BS" => array('eng' => "Bahamas", 'native' => ""),
        "BH" => array('eng' => "Bahrain", 'native' => "‫البحرين"),
        "BD" => array('eng' => "Bangladesh", 'native' => "বাংলাদেশ"),
        "BB" => array('eng' => "Barbados", 'native' => ""),
        "BY" => array('eng' => "Belarus", 'native' => "Беларусь"),
        "BE" => array('eng' => "Belgium", 'native' => "België"),
        "BZ" => array('eng' => "Belize", 'native' => ""),
        "BJ" => array('eng' => "Benin", 'native' => "Bénin"),
        "BM" => array('eng' => "Bermuda", 'native' => ""),
        "BT" => array('eng' => "Bhutan", 'native' => "འབྲུག"),
        "BO" => array('eng' => "Bolivia", 'native' => ""),
        "BA" => array('eng' => "Bosnia and Herzegovina", 'native' => "Босна и Херцеговина"),
        "BW" => array('eng' => "Botswana", 'native' => ""),
        "BV" => array('eng' => "Bouvet Island", 'native' => ""),
        "BR" => array('eng' => "Brazil", 'native' => "Brasil"),
        "IO" => array('eng' => "British Indian Ocean Territory", 'native' => ""),
        "VG" => array('eng' => "British Virgin Islands", 'native' => ""),
        "BN" => array('eng' => "Brunei", 'native' => ""),
        "BG" => array('eng' => "Bulgaria", 'native' => "България"),
        "BF" => array('eng' => "Burkina Faso", 'native' => ""),
        "BI" => array('eng' => "Burundi", 'native' => "Uburundi"),
        "KH" => array('eng' => "Cambodia", 'native' => "កម្ពុជា"),
        "CM" => array('eng' => "Cameroon", 'native' => "Cameroun"),
        "CA" => array('eng' => "Canada", 'native' => ""),
        "IC" => array('eng' => "Canary Islands", 'native' => "islas Canarias"),
        "CV" => array('eng' => "Cape Verde", 'native' => "Kabu Verdi"),
        "BQ" => array('eng' => "Caribbean Netherlands", 'native' => ""),
        "KY" => array('eng' => "Cayman Islands", 'native' => ""),
        "CF" => array('eng' => "Central African Republic", 'native' => "République centrafricaine"),
        "EA" => array('eng' => "Ceuta and Melilla", 'native' => "Ceuta y Melilla"),
        "TD" => array('eng' => "Chad", 'native' => "Tchad"),
        "CL" => array('eng' => "Chile", 'native' => ""),
        "CN" => array('eng' => "China", 'native' => "中国"),
        "CX" => array('eng' => "Christmas Island", 'native' => ""),
        "CP" => array('eng' => "Clipperton Island", 'native' => ""),
        "CC" => array('eng' => "Cocos (Keeling) Islands", 'native' => "Kepulauan Cocos (Keeling)"),
        "CO" => array('eng' => "Colombia", 'native' => ""),
        "KM" => array('eng' => "Comoros", 'native' => "‫جزر القمر"),
        "CD" => array('eng' => "Congo (DRC)", 'native' => "Jamhuri ya Kidemokrasia ya Kongo"),
        "CG" => array('eng' => "Congo (Republic)", 'native' => "Congo-Brazzaville"),
        "CK" => array('eng' => "Cook Islands", 'native' => ""),
        "CR" => array('eng' => "Costa Rica", 'native' => ""),
        "CI" => array('eng' => "Côte d’Ivoire", 'native' => ""),
        "HR" => array('eng' => "Croatia", 'native' => "Hrvatska"),
        "CU" => array('eng' => "Cuba", 'native' => ""),
        "CW" => array('eng' => "Curaçao", 'native' => ""),
        "CY" => array('eng' => "Cyprus", 'native' => "Κύπρος"),
        "CZ" => array('eng' => "Czech Republic", 'native' => "Česká republika"),
        "DK" => array('eng' => "Denmark", 'native' => "Danmark"),
        "DG" => array('eng' => "Diego Garcia", 'native' => ""),
        "DJ" => array('eng' => "Djibouti", 'native' => ""),
        "DM" => array('eng' => "Dominica", 'native' => ""),
        "DO" => array('eng' => "Dominican Republic", 'native' => "República Dominicana"),
        "EC" => array('eng' => "Ecuador", 'native' => ""),
        "EG" => array('eng' => "Egypt", 'native' => "‫مصر"),
        "SV" => array('eng' => "El Salvador", 'native' => ""),
        "GQ" => array('eng' => "Equatorial Guinea", 'native' => "Guinea Ecuatorial"),
        "ER" => array('eng' => "Eritrea", 'native' => ""),
        "EE" => array('eng' => "Estonia", 'native' => "Eesti"),
        "ET" => array('eng' => "Ethiopia", 'native' => ""),
        "FK" => array('eng' => "Falkland Islands", 'native' => "Islas Malvinas"),
        "FO" => array('eng' => "Faroe Islands", 'native' => "Føroyar"),
        "FJ" => array('eng' => "Fiji", 'native' => ""),
        "FI" => array('eng' => "Finland", 'native' => "Suomi"),
        "FR" => array('eng' => "France", 'native' => ""),
        "GF" => array('eng' => "French Guiana", 'native' => "Guyane française"),
        "PF" => array('eng' => "French Polynesia", 'native' => "Polynésie française"),
        "TF" => array('eng' => "French Southern Territories", 'native' => "Terres australes françaises"),
        "GA" => array('eng' => "Gabon", 'native' => ""),
        "GM" => array('eng' => "Gambia", 'native' => ""),
        "GE" => array('eng' => "Georgia", 'native' => "საქართველო"),
        "DE" => array('eng' => "Germany", 'native' => "Deutschland"),
        "GH" => array('eng' => "Ghana", 'native' => "Gaana"),
        "GI" => array('eng' => "Gibraltar", 'native' => ""),
        "GR" => array('eng' => "Greece", 'native' => "Ελλάδα"),
        "GL" => array('eng' => "Greenland", 'native' => "Kalaallit Nunaat"),
        "GD" => array('eng' => "Grenada", 'native' => ""),
        "GP" => array('eng' => "Guadeloupe", 'native' => ""),
        "GU" => array('eng' => "Guam", 'native' => ""),
        "GT" => array('eng' => "Guatemala", 'native' => ""),
        "GG" => array('eng' => "Guernsey", 'native' => ""),
        "GN" => array('eng' => "Guinea", 'native' => "Guinée"),
        "GW" => array('eng' => "Guinea-Bissau", 'native' => "Guiné Bissau"),
        "GY" => array('eng' => "Guyana", 'native' => ""),
        "HT" => array('eng' => "Haiti", 'native' => ""),
        "HM" => array('eng' => "Heard & McDonald Islands", 'native' => ""),
        "HN" => array('eng' => "Honduras", 'native' => ""),
        "HK" => array('eng' => "Hong Kong", 'native' => "香港"),
        "HU" => array('eng' => "Hungary", 'native' => "Magyarország"),
        "IS" => array('eng' => "Iceland", 'native' => "Ísland"),
        "IN" => array('eng' => "India", 'native' => "भारत"),
        "ID" => array('eng' => "Indonesia", 'native' => ""),
        "IR" => array('eng' => "Iran", 'native' => "‫ایران"),
        "IQ" => array('eng' => "Iraq", 'native' => "‫العراق"),
        "IE" => array('eng' => "Ireland", 'native' => ""),
        "IM" => array('eng' => "Isle of Man", 'native' => ""),
        "IL" => array('eng' => "Israel", 'native' => "‫ישראל"),
        "IT" => array('eng' => "Italy", 'native' => "Italia"),
        "JM" => array('eng' => "Jamaica", 'native' => ""),
        "JP" => array('eng' => "Japan", 'native' => "日本"),
        "JE" => array('eng' => "Jersey", 'native' => ""),
        "JO" => array('eng' => "Jordan", 'native' => "‫الأردن"),
        "KZ" => array('eng' => "Kazakhstan", 'native' => "Казахстан"),
        "KE" => array('eng' => "Kenya", 'native' => ""),
        "KI" => array('eng' => "Kiribati", 'native' => ""),
        "XK" => array('eng' => "Kosovo", 'native' => "Kosovë"),
        "KW" => array('eng' => "Kuwait", 'native' => "‫الكويت"),
        "KG" => array('eng' => "Kyrgyzstan", 'native' => "Кыргызстан"),
        "LA" => array('eng' => "Laos", 'native' => "ລາວ"),
        "LV" => array('eng' => "Latvia", 'native' => "Latvija"),
        "LB" => array('eng' => "Lebanon", 'native' => "‫لبنان"),
        "LS" => array('eng' => "Lesotho", 'native' => ""),
        "LR" => array('eng' => "Liberia", 'native' => ""),
        "LY" => array('eng' => "Libya", 'native' => "‫ليبيا"),
        "LI" => array('eng' => "Liechtenstein", 'native' => ""),
        "LT" => array('eng' => "Lithuania", 'native' => "Lietuva"),
        "LU" => array('eng' => "Luxembourg", 'native' => ""),
        "MO" => array('eng' => "Macau", 'native' => "澳門"),
        "MK" => array('eng' => "Macedonia (FYROM)", 'native' => "Македонија"),
        "MG" => array('eng' => "Madagascar", 'native' => "Madagasikara"),
        "MW" => array('eng' => "Malawi", 'native' => ""),
        "MY" => array('eng' => "Malaysia", 'native' => ""),
        "MV" => array('eng' => "Maldives", 'native' => ""),
        "ML" => array('eng' => "Mali", 'native' => ""),
        "MT" => array('eng' => "Malta", 'native' => ""),
        "MH" => array('eng' => "Marshall Islands", 'native' => ""),
        "MQ" => array('eng' => "Martinique", 'native' => ""),
        "MR" => array('eng' => "Mauritania", 'native' => "‫موريتانيا"),
        "MU" => array('eng' => "Mauritius", 'native' => "Moris"),
        "YT" => array('eng' => "Mayotte", 'native' => ""),
        "MX" => array('eng' => "Mexico", 'native' => ""),
        "FM" => array('eng' => "Micronesia", 'native' => ""),
        "MD" => array('eng' => "Moldova", 'native' => "Republica Moldova"),
        "MC" => array('eng' => "Monaco", 'native' => ""),
        "MN" => array('eng' => "Mongolia", 'native' => "Монгол"),
        "ME" => array('eng' => "Montenegro", 'native' => "Crna Gora"),
        "MS" => array('eng' => "Montserrat", 'native' => ""),
        "MA" => array('eng' => "Morocco", 'native' => "‫المغرب"),
        "MZ" => array('eng' => "Mozambique", 'native' => "Moçambique"),
        "MM" => array('eng' => "Myanmar (Burma)", 'native' => "မြန်မာ"),
        "NA" => array('eng' => "Namibia", 'native' => "Namibië"),
        "NR" => array('eng' => "Nauru", 'native' => ""),
        "NP" => array('eng' => "Nepal", 'native' => "नेपाल"),
        "NL" => array('eng' => "Netherlands", 'native' => "Nederland"),
        "NC" => array('eng' => "New Caledonia", 'native' => "Nouvelle-Calédonie"),
        "NZ" => array('eng' => "New Zealand", 'native' => ""),
        "NI" => array('eng' => "Nicaragua", 'native' => ""),
        "NE" => array('eng' => "Niger", 'native' => "Nijar"),
        "NG" => array('eng' => "Nigeria", 'native' => ""),
        "NU" => array('eng' => "Niue", 'native' => ""),
        "NF" => array('eng' => "Norfolk Island", 'native' => ""),
        "MP" => array('eng' => "Northern Mariana Islands", 'native' => ""),
        "KP" => array('eng' => "North Korea", 'native' => "조선 민주주의 인민 공화국"),
        "NO" => array('eng' => "Norway", 'native' => "Norge"),
        "OM" => array('eng' => "Oman", 'native' => "‫عُمان"),
        "PK" => array('eng' => "Pakistan", 'native' => "‫پاکستان"),
        "PW" => array('eng' => "Palau", 'native' => ""),
        "PS" => array('eng' => "Palestine", 'native' => "‫فلسطين"),
        "PA" => array('eng' => "Panama", 'native' => ""),
        "PG" => array('eng' => "Papua New Guinea", 'native' => ""),
        "PY" => array('eng' => "Paraguay", 'native' => ""),
        "PE" => array('eng' => "Peru", 'native' => "Perú"),
        "PH" => array('eng' => "Philippines", 'native' => ""),
        "PN" => array('eng' => "Pitcairn Islands", 'native' => ""),
        "PL" => array('eng' => "Poland", 'native' => "Polska"),
        "PT" => array('eng' => "Portugal", 'native' => ""),
        "PR" => array('eng' => "Puerto Rico", 'native' => ""),
        "QA" => array('eng' => "Qatar", 'native' => "‫قطر"),
        "RE" => array('eng' => "Réunion", 'native' => "La Réunion"),
        "RO" => array('eng' => "Romania", 'native' => "România"),
        "RU" => array('eng' => "Russia", 'native' => "Россия"),
        "RW" => array('eng' => "Rwanda", 'native' => ""),
        "BL" => array('eng' => "Saint Barthélemy", 'native' => "Saint-Barthélemy"),
        "SH" => array('eng' => "Saint Helena", 'native' => ""),
        "KN" => array('eng' => "Saint Kitts and Nevis", 'native' => ""),
        "LC" => array('eng' => "Saint Lucia", 'native' => ""),
        "MF" => array('eng' => "Saint Martin", 'native' => ""),
        "PM" => array('eng' => "Saint Pierre and Miquelon", 'native' => "Saint-Pierre-et-Miquelon"),
        "WS" => array('eng' => "Samoa", 'native' => ""),
        "SM" => array('eng' => "San Marino", 'native' => ""),
        "ST" => array('eng' => "São Tomé and Príncipe", 'native' => "São Tomé e Príncipe"),
        "SA" => array('eng' => "Saudi Arabia", 'native' => "‫المملكة العربية السعودية"),
        "SN" => array('eng' => "Senegal", 'native' => "Sénégal"),
        "RS" => array('eng' => "Serbia", 'native' => "Србија"),
        "SC" => array('eng' => "Seychelles", 'native' => ""),
        "SL" => array('eng' => "Sierra Leone", 'native' => ""),
        "SG" => array('eng' => "Singapore", 'native' => ""),
        "SX" => array('eng' => "Sint Maarten", 'native' => ""),
        "SK" => array('eng' => "Slovakia", 'native' => "Slovensko"),
        "SI" => array('eng' => "Slovenia", 'native' => "Slovenija"),
        "SB" => array('eng' => "Solomon Islands", 'native' => ""),
        "SO" => array('eng' => "Somalia", 'native' => "Soomaaliya"),
        "ZA" => array('eng' => "South Africa", 'native' => ""),
        "GS" => array('eng' => "South Georgia & South Sandwich Islands", 'native' => ""),
        "KR" => array('eng' => "South Korea", 'native' => "대한민국"),
        "SS" => array('eng' => "South Sudan", 'native' => "‫جنوب السودان"),
        "ES" => array('eng' => "Spain", 'native' => "España"),
        "LK" => array('eng' => "Sri Lanka", 'native' => "ශ්‍රී ලංකාව"),
        "VC" => array('eng' => "St. Vincent & Grenadines", 'native' => ""),
        "SD" => array('eng' => "Sudan", 'native' => "‫السودان"),
        "SR" => array('eng' => "Surieng", 'native' => ""),
        "SJ" => array('eng' => "Svalbard and Jan Mayen", 'native' => "Svalbard og Jan Mayen"),
        "SZ" => array('eng' => "Swaziland", 'native' => ""),
        "SE" => array('eng' => "Sweden", 'native' => "Sverige"),
        "CH" => array('eng' => "Switzerland", 'native' => "Schweiz"),
        "SY" => array('eng' => "Syria", 'native' => "‫سوريا"),
        "TW" => array('eng' => "Taiwan", 'native' => "台灣"),
        "TJ" => array('eng' => "Tajikistan", 'native' => ""),
        "TZ" => array('eng' => "Tanzania", 'native' => ""),
        "TH" => array('eng' => "Thailand", 'native' => "ไทย"),
        "TL" => array('eng' => "Timor-Leste", 'native' => ""),
        "TG" => array('eng' => "Togo", 'native' => ""),
        "TK" => array('eng' => "Tokelau", 'native' => ""),
        "TO" => array('eng' => "Tonga", 'native' => ""),
        "TT" => array('eng' => "Trinidad and Tobago", 'native' => ""),
        "TA" => array('eng' => "Tristan da Cunha", 'native' => ""),
        "TN" => array('eng' => "Tunisia", 'native' => "‫تونس"),
        "TR" => array('eng' => "Turkey", 'native' => "Türkiye"),
        "TM" => array('eng' => "Turkmenistan", 'native' => ""),
        "TC" => array('eng' => "Turks and Caicos Islands", 'native' => ""),
        "TV" => array('eng' => "Tuvalu", 'native' => ""),
        "UM" => array('eng' => "U.S. Outlying Islands", 'native' => ""),
        "VI" => array('eng' => "U.S. Virgin Islands", 'native' => ""),
        "UG" => array('eng' => "Uganda", 'native' => ""),
        "UA" => array('eng' => "Ukraine", 'native' => "Україна"),
        "AE" => array('eng' => "United Arab Emirates", 'native' => "‫الإمارات العربية المتحدة"),
        "GB" => array('eng' => "United Kingdom", 'native' => ""),
        "US" => array('eng' => "United States", 'native' => ""),
        "UY" => array('eng' => "Uruguay", 'native' => ""),
        "UZ" => array('eng' => "Uzbekistan", 'native' => "Oʻzbekiston"),
        "VU" => array('eng' => "Vanuatu", 'native' => ""),
        "VA" => array('eng' => "Vatican City", 'native' => "Città del Vaticano"),
        "VE" => array('eng' => "Venezuela", 'native' => ""),
        "VN" => array('eng' => "Vietnam", 'native' => "Việt Nam"),
        "WF" => array('eng' => "Wallis and Futuna", 'native' => ""),
        "EH" => array('eng' => "Western Sahara", 'native' => "‫الصحراء الغربية"),
        "YE" => array('eng' => "Yemen", 'native' => "‫اليمن"),
        "ZM" => array('eng' => "Zambia", 'native' => ""),
        "ZW" => array('eng' => "Zimbabwe", 'native' => "")
    ];
    
    public static function getCountriesWithNativeNames() {
        $countries = [];
        
        foreach (self::$countries as $key => $value) {
            $countries[$key] = $value['native'] !== '' ? $value['native'] : $value['eng'];
        } 
        
        return $countries;
    }
    
    public static function getCountriesWithEnglishNames() {
        $countries = [];
        
        foreach (self::$countries as $key => $value) {
            $countries[$key] = $value['eng'];
        } 
        
        return $countries;
    }

    public static function getName($iso) {
        if (!isset(self::$countries[$iso])) {
            return $iso;
        }
        return self::$countries[$iso]['native'] ? self::$countries[$iso]['native'] : self::$countries[$iso]['eng'];
    }
}
