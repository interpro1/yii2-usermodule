<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'ID' => '',
    'Address' => 'Adres',
    'Apartment Number' => 'Numer mieszkania',
    'City' => 'Miasto',
    'Company address' => 'Adres firmowy',
    'Country' => 'Państwo',
    'Create address' => 'Dodaj adres',
    'Id User' => 'Id użytkownika',
    'Main address' => 'Główny adres',
    'Nip' => 'Numer NIP',
    'Number' => 'Numer budynku',
    'Required only for company' => 'Wymagane tylko dla firm',
    'Payment address' => 'Adres do faktury',
    'Phone' => 'Telefon',
    'Post Code' => 'Kod pocztowy',
    'Recipient' => 'Adresat (imię i nazwisko / nazwa firmy)',
    'Shippment address' => 'Adres do wysyłki',
    'Street' => 'Ulica',
    'Submit' => 'Zapisz',
    'Type' => 'Typ',
    'Update address' => 'Zmień adres',
    'User does not exist.' => 'Użytkownik nie istnieje',
    'Save changes' => 'Zapisz zmiany',
    'Select country' => 'Wybierz kraj',
    'I want to receive a VAT invoice' => 'Chcę otrzymać fakturę VAT',

];
