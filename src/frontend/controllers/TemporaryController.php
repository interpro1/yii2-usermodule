<?php

namespace interPro\userModule\frontend\controllers;

use interPro\userModule\common\models\InvoiceAddress;
use Yii;
use interPro\userModule\common\models\User;
use interPro\userModule\common\models\Address;
use yii\web\Controller;
use yii\filters\AccessControl;

class TemporaryController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ]
                ],
            ],
        ];
    }

    public function actionCreate($from = null)
    {
        $model = new User([
            'userType' => User::TYPE_TEMPORARY
        ]);
        $model->setScenario(User::SCENARIO_TEMPORARY);
        $address = new Address([
            'country' => 'PL',
            'scenario' => Address::SCENARIO_SHIPPING,
            'type' => Address::TYPE_SHIPPMENT
        ]);
        $paymentAddress = new InvoiceAddress([
            'country' => 'PL',
            'scenario' => Address::SCENARIO_DEFAULT,
            'type' => Address::TYPE_PAYMENT
        ]);

        if (Yii::$app->request->isPost) {
            $model->setAttributes(Yii::$app->request->post()["User"]);
            $address->setAttributes(Yii::$app->request->post()["Address"]);
            $paymentAddress->setAttributes(Yii::$app->request->post()["InvoiceAddress"]);
        } elseif (Yii::$app->session->get(User::TEMPORARY_USER_KEY) !== null) {
            $data = Yii::$app->session->get(User::TEMPORARY_USER_KEY);
            $model->setAttributes($data["User"]);
            $address->setAttributes($data["Address"]);

            if (key_exists('InvoiceAddress', $data)) {
                $paymentAddress->setAttributes($data["InvoiceAddress"]);
            }
        }

        if (Yii::$app->request->isPost && $model->validate()) {
            $data = Yii::$app->request->post();
            unset($data["_csrf"]);
            $data['Address']['phone'] = $data['User']['phone'];

            if ($data['InvoiceAddress']['name'] === '') {
                unset($data['InvoiceAddress']);
            }

            Yii::$app->session->set('temporaryUser', $data);

            if ($from) {
                return $this->redirect($from);
            }

            return $this->goHome();
        } else {
            return $this->render('form', [
                'model' => $model,
                'address' => $address,
                'paymentAddress' => $paymentAddress
            ]);
        }
    }
}