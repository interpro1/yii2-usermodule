<?php

namespace interPro\userModule\frontend\controllers;

use Yii;
use interPro\userModule\common\models\User;
use interPro\userModule\common\models\LoginForm;
use interPro\userModule\common\models\Address;
use yii\web\Controller;
use yii\filters\AccessControl;
use interPro\userModule\frontend\models\PasswordResetRequestForm;
use interPro\userModule\frontend\models\ResetPasswordForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;

/**
 * DefaultController 
 */
class DefaultController extends Controller
{
    
    public $defaultAction = 'my-account';
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['signup', 'login', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'my-account', 'address', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new User();
        $model->setScenario('register');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->getUser()->login($model)) {
                return $this->goHome();
            }
            
        } else {
            return $this->render('signup', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionLogin($service = null, $from = null)
    {
        if (!\Yii::$app->user->isGuest) {
            return $from ? $this->redirect($from) : $this->goHome();
        }

        if ($service) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($service);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('/user/login'));

            try {
                if ($eauth->authenticate()) {

                    $identity = User::findByEAuth($eauth);

                    Yii::$app->getUser()->login($identity, 3600 * 24 * 30);
                    // special redirect with closing popup window
                    if (is_null($identity->email)) {
                        Yii::$app->session->setFlash('info', Yii::t('user', 'Please enter your email address.'));
                        $eauth->redirect(['/user/update', 'redirect' => $eauth->getRedirectUrl()]);
                    } else {
                        $eauth->redirect();
                    }
                }
                else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
                $eauth->redirect($eauth->getCancelUrl());
            }
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $from ? $this->redirect($from) : $this->goHome();
        } else {
            return $this->render('login', [
                'model' => $model,
                'from' => $from
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout(false);

        return $this->goHome();
    }
    
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('user','Check your email for further instructions.'));

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('user','Sorry, we are unable to reset password for email provided.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('user','New password was saved.'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionMyAccount()
    {
        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->user->getId()),
        ]);
    }
    
    public function actionUpdate($redirect = null) {
        
        $model = $this->findModel(Yii::$app->user->getId());
        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($redirect ? $redirect : ['my-account']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionAddress($id = null, $from = null, $type = null) {
        $model = $this->findAddressById($id);
        
        if($type) {
            $model->type = $type;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($from) {
                return $this->redirect([$from, 'type' => $model->type, 'id' => $model->id]);
            }
            
            return $this->redirect(['my-account']);
        } else {
            return $this->render('address', [
                'model' => $model,
            ]);
        }

        return $this->render('address', [
                    'model' => $model
        ]);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            return Yii::$app->getUser()->getIdentity();
        }
    }
    
    protected function findAddressById($id) {
        if (isset($id) && ($model = Address::findOne(['idUser' => Yii::$app->user->getId(), 'id' => $id])) !== null) {
            return $model;
        } else {
            return new Address(['idUser' => Yii::$app->user->getId()]);
        }
    }
}
