<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use interPro\userModule\common\models\Address;
use interPro\userModule\common\models\InvoiceAddress;

/* @var $this yii\web\View */
/* @var $model interPro\userModule\common\models\User */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="user-form">

    <h1>Dane do wysyłki</h1>
    <?php $form = ActiveForm::begin(); ?>

    <?= $this->render('_address', [
        'form' => $form,
        'model' => $address,
        'user' => $model
    ]) ?>

    <?= $this->render('_address_invoice', [
        'form' => $form,
        'model' => $paymentAddress
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('all', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
