<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use interPro\userModule\common\models\Address;
use interPro\userModule\common\dictionary\Countries;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model interPro\userModule\common\models\Address */
/* @var $form ActiveForm */

?>
<p>
    <label>
        <?= Html::checkbox('add-invoice', false, [
            'id' => 'add-invoice'
        ]) ?>
        <?= Yii::t('address', 'I want to receive a VAT invoice'); ?>
    </label>
</p>
    <p>&nbsp;</p>
<div id="address-temporary-invoice" style="display: none">
    <h3><?= Yii::t('address', 'Payment address'); ?></h3>
    <div class="row">
        <div class="col-md-4">

            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'nip')->label(Yii::t('address', 'Nip') . ' <small>(' . Yii::t('address', 'Required only for company') . ')</small>') ?>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'street') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'number') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'apartmentNumber') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <?= $form->field($model, 'city') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'postCode') ?>
                </div>
                <div class="col-md-4">
                    <?=
                        $form->field($model, 'country')->widget(Select2::classname(), [
                            'data' => Countries::getCountriesWithNativeNames(),
                            'options' => [
                                'placeholder' => Yii::t('address', 'Select country'),
                                'id' => 'address-country-invoice'
                            ],
                        ]);
                        $form->field($model, 'country')
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    $this->registerJs("
        $('#add-invoice').on('change', function (e) {
            if (this.checked) {
                $('#address-temporary-invoice').show();
            } else {
                $('#address-temporary-invoice').hide();
            }
        });
    ");