<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use interPro\userModule\common\models\Address;
use interPro\userModule\common\dictionary\Countries;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model interPro\userModule\common\models\Address */
/* @var $form ActiveForm */

?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($user, 'firstName')->textInput(['maxlength' => 100]) ?>
            <?= $form->field($user, 'lastName')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($user, 'phone')->textInput(['maxlength' => 15]) ?>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'name') ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'street') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'number') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'apartmentNumber') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <?= $form->field($model, 'city') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'postCode') ?>
                </div>
                <div class="col-md-4">
                    <?=
                        $form->field($model, 'country')->widget(Select2::classname(), [
                            'data' => Countries::getCountriesWithNativeNames(),
                            'options' => ['placeholder' => Yii::t('address', 'Select country')],
                        ]);
                        $form->field($model, 'country')
                    ?>
                </div>
            </div>
        </div>
    </div>