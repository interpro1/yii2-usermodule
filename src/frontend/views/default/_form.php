<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'firstName')->textInput(['maxlength' => 100]) ?>
            <?= $form->field($model, 'lastName')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'phone')->textInput(['maxlength' => 15]) ?>
        </div>
        <div class="col-md-6">
            <?php
                if (!Yii::$app->getUser()->identity->isSocialUser()) {
                    echo $form->field($model, 'password')->passwordInput(['value' => '']);

                    echo $form->field($model, 'repeatpassword')->passwordInput(['value' => '']);
                }
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('user', 'Register') : Yii::t('all', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
