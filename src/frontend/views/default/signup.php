<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model interPro\userModule\common\models\User */

$this->title = Yii::t('user', 'Registration');
$this->params['breadcrumbs'][] = $this->title;
?>
<article class="user-create">

    <div class="row">
        <div class="col-md-4">
            <h1 class="green"><?= Html::encode($this->title) ?></h1>
            <?= $this->render('_register_form', [
                'model' => $model,
            ]) ?>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-6">
            <h2 class="blue"><?= Yii::t('user', 'Already you have an account?'); ?></h2>
            <p><?= Html::a(Yii::t('user', 'Go to login'), Yii::getAlias('@web').'/user/login', [
                    'class' => 'btn btn-success'
                ]); ?></p>
        </div>
    </div>

</article>
