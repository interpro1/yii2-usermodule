<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model interPro\userModule\common\models\User */

$this->title = Yii::t('user', 'My account');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    
    <?php if (count(\Yii::$app->controller->module->userMenu)) {
    ?>
        <p class="pull-right">
            <?php foreach (\Yii::$app->controller->module->userMenu as $menu) {
                echo Html::a(is_array($menu['label']) ? Yii::t($menu['label']['category'], $menu['label']['name']) : $menu['label'], $menu['url'], ['class' => key_exists('class', $menu) ? $menu['class'] : 'btn-default btn']);
            } ?>
        </p>
    <?php
    }
    ?>
    
    <h1><?= Html::encode($this->title) ?> 
        <?= Html::a(Yii::t('user', 'Edit data'), ['/user/update'], ['class' => 'btn btn-primary']) ?>
    </h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'firstName',
            'lastName',
            'email:email',
            'phone',
            'createdDate'
        ],
    ]);

    echo '<h3>' . Yii::t('user', 'Your addresses') . '</h3>';

    $dataAddressProvider = new ActiveDataProvider([
        'query' => $model->getAddresses()
    ]);

    echo GridView::widget([
        'dataProvider' => $dataAddressProvider,
        'layout' => "{items}\n{pager}",
        'columns' => [
            [
                'attribute' => 'user',
                'label' => Yii::t('user', 'Type'),
                'format' => 'html',
                'value' => function ($model) {
                    return $model->getType();
                },
            ],
            'name',
            [
                'attribute' => 'user',
                'label' => Yii::t('address', 'Address'),
                'format' => 'html',
                'value' => function ($model) {
                    return $model->getPreformatted();
                },
            ],
            'nip',
            'phone',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('address', 'Update address'),
                            'href' => '/user/address?id=' . $model->id
                        ];

                        return Html::a('<span class="glyphicon glyphicon-edit"></span>', null, $options);
                    }
                        ]
                    ],
                ],
            ]);
            ?>
    <div class="form-group">
        <?= Html::a(Yii::t('address', 'Create address'), '/user/address', ['class' => 'btn btn-primary']) ?>
    </div>

</div>
