<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model interPro\userModule\common\models\LoginForm */

$this->title = Yii::t('user', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<article class="site-login">

    <div class="row">
        <div class="col-lg-4">
            <h1 class="green"><?= Html::encode($this->title) ?></h1>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <div style="color:#999;margin:1em 0">
                    <?= Yii::t('user', 'If you forgot your password you can'); ?> <?= Html::a(Yii::t('user', 'reset it'), ['/user/request-password-reset']) ?>.
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('user', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>

            <?php if (count(Yii::$app->controller->module->services)) { ?>
                <p class="lead"><?= Yii::t('user', 'Click on the logo below to log in using the service:'); ?></p>
                <?php echo \nodge\eauth\Widget::widget([
                    'action' => '/user/login',
                    'assetBundle' => 'interPro\userModule\frontend\assets\AuthWidgetAssets'
                ]); ?>
            <?php } ?>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-6">
            <h2 class="blue"><?= Yii::t('user', 'You do not have an account yet?'); ?></h2>
            <p><?= Html::a(Yii::t('user', 'Go to register'), ['/user/signup'], [
                'class' => 'btn btn-success'
            ]); ?></p>

            <p>&nbsp;</p>
            <?php
                if (Yii::$app->controller->module->enableTemporaryUsers) {
            ?>
                <h2 class="blue"><?= Yii::t('user', 'You do not want to register?'); ?></h2>
                <p>
                    <?=
                        Html::a(Yii::t('user', 'Go shopping without registration'), [
                            '/user/temporary/create',
                            'from' => isset($from) ? $from : ''
                        ], [
                            'class' => 'btn btn-success'
                        ]);
                    ?>
                </p>
            <?php
                }
            ?>
        </div>
    </div>
</article>
