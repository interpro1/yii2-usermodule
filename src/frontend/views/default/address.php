<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use interPro\userModule\common\models\Address;
use interPro\userModule\common\dictionary\Countries;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model interPro\userModule\common\models\Address */
/* @var $form ActiveForm */

$this->title = $model->isNewRecord ? Yii::t('address', 'Create address') : Yii::t('address', 'Update address');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'My account'), 'url' => ['my-account']];
$this->params['breadcrumbs'][] = $this->title;
?>
<article class="user-address">

    <h1 class="green"><?= $this->title ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">

            <?= $form->field($model, 'type')->dropDownList(Address::getTypesList()) ?>
            <?= $form->field($model, 'nip')->label(Yii::t('address', 'Nip') . ' <small>(' . Yii::t('address', 'Required only for company') . ')</small>') ?>
            <?= $form->field($model, 'phone') ?>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'name') ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'street') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'number') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'apartmentNumber') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <?= $form->field($model, 'city') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'postCode') ?>
                </div>
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'country')->widget(Select2::classname(), [
                        'data' => Countries::getCountriesWithNativeNames(),
                        'options' => ['placeholder' => Yii::t('address', 'Select country')],
                    ]);
                    $form->field($model, 'country')
                    ?>
                </div>
            </div>
        </div>


    </div>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('address', 'Submit'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</article><!-- user-address -->
