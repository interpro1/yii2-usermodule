<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model interPro\userModule\common\models\User */

$this->title = Yii::t('user', 'Update {modelClass}: ', [
    'modelClass' => 'User',
]) . ' ' . $model->firstName . ' ' . $model->lastName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'My account'), 'url' => ['my-account']];
$this->params['breadcrumbs'][] = Yii::t('user', 'Update');
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
