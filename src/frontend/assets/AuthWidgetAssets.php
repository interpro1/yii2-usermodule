<?php

namespace interPro\userModule\frontend\assets;

use nodge\eauth\assets\WidgetAssetBundle;

class AuthWidgetAssets extends WidgetAssetBundle
{
    public $css = [];
}