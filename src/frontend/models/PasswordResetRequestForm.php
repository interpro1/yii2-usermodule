<?php
namespace interPro\userModule\frontend\models;

use interPro\userModule\common\models\User;
use yii\base\Model;
use Yii;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\interPro\userModule\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => Yii::t('user', 'There is no user with such email.')
            ],
        ];
    }
    
    
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('all', 'E-mail'),
        ];
    }
    

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if ($user) {
            if (!User::isPasswordResetTokenValid($user->passwordResetToken)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save()) {
                return \Yii::$app->mailer->compose([
                        'html' => '@vendor/interpro/yii2-userModule/src/common/mail/html/password-reset-token', 
                        'text' => '@vendor/interpro/yii2-userModule/src/common/mail/text/password-reset-token'
                    ], ['user' => $user])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name])
                    ->setTo($this->email)
                    ->setSubject(Yii::t('user', 'Password reset for ') . \Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}
