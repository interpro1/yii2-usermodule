<?php

namespace interPro\userModule\frontend;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'interPro\userModule\frontend\controllers';
    public $notifyAdministrator = false;
    public $enableTemporaryUsers = false;
    public $userMenu = [];
    public $services = [];

    public function init()
    {
        parent::init();
        Yii::$app->getUrlManager()->addRules(
            [
                'user/<action>' => 'user/default/<action>'
            ],
            true
        );

        $this->registerTranslations();

        if (count($this->services)) {
            $services = [];
            foreach ($this->services  as $key => $service) {
                switch ($key) {
                    case 'google':
                        $services['google'] = [
                            // register your app here: https://code.google.com/apis/console/
                            'class' => 'interPro\userModule\common\services\OAuth2\GoogleService',
                            'clientId' => $service['clientId'],
                            'clientSecret' => $service['secretKey'],
                            'title' => 'Google'
                        ];
                        break;
                    case 'twitter':
                        $services['twitter'] = [
                            // register your app here: https://dev.twitter.com/apps/new
                            'class' => 'interPro\userModule\common\services\OAuth1\TwitterService',
                            'key' => $service['clientId'],
                            'secret' => $service['secretKey']
                        ];
                        break;
                    case 'facebook':
                        $services['facebook'] = [
                            // register your app here: https://developers.facebook.com/apps/
                            'class' => 'interPro\userModule\common\services\OAuth2\FacebookService',
                            'clientId' => $service['clientId'],
                            'clientSecret' => $service['secretKey'],
                        ];
                        break;
                    case 'instagram':
                        $services['instagram'] = [
                            // register your app here: https://instagram.com/developer/register/
                            'class' => 'interPro\userModule\common\services\OAuth2\InstagramService',
                            'clientId' => 'd7ae6778f0d54a8a9340f0ba9f708d2a',
                            'clientSecret' => '174a9e6f659849c7a286cde508200a50',
                        ];
                        break;
                    default:
                        throw new \InvalidArgumentException('Unknown provider: ' . $key);
                }
            }

            Yii::$app->setComponents([
                'eauth' => [
                    'class' => 'nodge\eauth\EAuth',
                    'popup' => true, // Use the popup window instead of redirecting.
                    'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
                    'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
                    'httpClient' => [
                        // uncomment this to use streams in safe_mode
                        //'useStreamsFallback' => true,
                    ],
                    'services' => $services
                ]
            ]);
        }
    }
    
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['user'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@vendor/interpro/yii2-userModule/src/common/messages'
        ];

        Yii::$app->i18n->translations['address'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@vendor/interpro/yii2-userModule/src/common/messages'
        ];

        Yii::$app->i18n->translations['eauth'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@vendor/interpro/yii2-userModule/src/common/messages'
        ];
    }
}
