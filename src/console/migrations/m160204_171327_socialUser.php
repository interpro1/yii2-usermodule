<?php

use yii\db\Schema;
use yii\db\Migration;

class m160204_171327_socialUser extends Migration
{
    public function up()
    {
        $this->addColumn('users', 'socialId', 'varchar(100)');
        $this->addColumn('users', 'photo', 'varchar(500)');
        $this->execute('ALTER TABLE  `users` CHANGE  `fullName`  `fullName` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ;
                        ALTER TABLE  `users` CHANGE  `email`  `email` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ;');
    }

    public function down()
    {
        $this->dropColumn('users', 'socialId');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
